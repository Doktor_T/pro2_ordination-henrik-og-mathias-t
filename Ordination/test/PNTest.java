package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import ordination.PN;

public class PNTest {

    private PN pn1;

    @Before
    public void setUp() {
        pn1 = new PN(LocalDate.of(2018, 07, 12), LocalDate.of(2018, 07, 18), 2);
    }

    @Test
    public void testPN() {
        // TC1
        PN pn2 = new PN(LocalDate.of(2018, 07, 12), LocalDate.of(2018, 07, 18), 2);
        assertNotNull(pn2);
        assertEquals(2, pn2.getAntalEnheder(), 0.001);
        assertEquals(LocalDate.of(2018, 07, 12), pn2.getStartDen());
        assertEquals(LocalDate.of(2018, 07, 18), pn2.getSlutDen());
    }

    @Test
    public void testGivDosis() {
        boolean b1 = pn1.givDosis(LocalDate.of(2018, 07, 12));
        boolean b2 = pn1.givDosis(LocalDate.of(2018, 07, 13));
        boolean b3 = pn1.givDosis(LocalDate.of(2018, 07, 18));

        // TC1
        assertEquals(true, b1);
        // TC2
        assertEquals(true, b2);
        // TC3
        assertEquals(true, b3);
        // TC4
        assertEquals(3, pn1.getDatoForDoser().size());
    }

    @Test
    public void testGivDosisUgyldig() {
        boolean b1 = pn1.givDosis(LocalDate.of(2018, 07, 11));
        boolean b2 = pn1.givDosis(LocalDate.of(2018, 07, 19));
        boolean b3 = pn1.givDosis(null);

        // TC1
        assertEquals(false, b1);
        // TC2
        assertEquals(false, b2);
        // TC3
        assertEquals(false, b3);
    }

    @Test
    public void testDoegnDosis() {
        pn1.givDosis(LocalDate.of(2018, 07, 12));
        pn1.givDosis(LocalDate.of(2018, 07, 13));
        pn1.givDosis(LocalDate.of(2018, 07, 18));
        pn1.givDosis(LocalDate.of(2018, 07, 16));
        pn1.givDosis(LocalDate.of(2018, 07, 16));
        pn1.givDosis(LocalDate.of(2018, 07, 14));
        pn1.givDosis(LocalDate.of(2018, 07, 18));

        // TC1
        assertEquals(2, pn1.doegnDosis(), 0.001);

    }

    @Test
    public void testSamletDosis() {
        pn1.givDosis(LocalDate.of(2018, 07, 12));
        pn1.givDosis(LocalDate.of(2018, 07, 13));
        pn1.givDosis(LocalDate.of(2018, 07, 18));
        pn1.givDosis(LocalDate.of(2018, 07, 16));
        pn1.givDosis(LocalDate.of(2018, 07, 16));
        pn1.givDosis(LocalDate.of(2018, 07, 14));
        pn1.givDosis(LocalDate.of(2018, 07, 18));

        // TC1
        assertEquals(14, pn1.samletDosis(), 0.001);
    }

    @Test
    public void testAntalDage() {
        pn1.givDosis(LocalDate.of(2018, 07, 12));
        pn1.givDosis(LocalDate.of(2018, 07, 13));
        pn1.givDosis(LocalDate.of(2018, 07, 16));
        pn1.givDosis(LocalDate.of(2018, 07, 16));
        pn1.givDosis(LocalDate.of(2018, 07, 14));

        // TC1
        assertEquals(5, pn1.antalDage(), 0.001);
    }
}
