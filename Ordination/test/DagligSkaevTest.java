package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligSkaev;
import ordination.Dosis;

public class DagligSkaevTest {

    private DagligSkaev ds1;

    @Before
    public void setUp() {
        ds1 = new DagligSkaev(LocalDate.of(2018, 07, 12), LocalDate.of(2018, 07, 18));
        ds1.opretDosis(LocalTime.of(8, 00), 2);
        ds1.opretDosis(LocalTime.of(12, 00), 3);
    }

    @Test
    public void testDagligSkaev() {
        // TC1
        assertNotNull(ds1);
        assertEquals(LocalDate.of(2018, 07, 12), ds1.getStartDen());
        assertEquals(LocalDate.of(2018, 07, 18), ds1.getSlutDen());
    }

    @Test
    public void testOpretDosis() {
        // TC1
        assertNotNull(ds1.getDoser());
        assertEquals(2, ds1.getDoser().size(), 0.001);
        Dosis d1 = ds1.opretDosis(LocalTime.of(9, 00), 2);
        Dosis d2 = ds1.opretDosis(LocalTime.of(15, 00), 1);
        assertNotNull(ds1.getDoser());
        assertEquals(4, ds1.getDoser().size(), 0.001);
        assertEquals(d1, ds1.getDoser().get(2));
        assertEquals(d2, ds1.getDoser().get(3));
    }

    @Test
    public void testDoegnDosis() {
        // TC1
        assertEquals(5, ds1.doegnDosis(), 0.001);
    }

    @Test
    public void testSamletDosis() {
        // TC1
        assertEquals(35, ds1.samletDosis(), 0.001);
    }
}
