package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;
import service.Service;

public class DagligFastTest {
    private Laegemiddel l3;
    private DagligFast d1, d2;

    @Before
    public void setUp() throws Exception {
        Service service = Service.getTestService();

        l3 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

        // Correct
        d1 = new DagligFast(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8));
        d2 = new DagligFast(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 6));

        d1.setLaegemiddel(l3);

        d1.createDosis(LocalTime.of(8, 00), 1);
        d1.createDosis(LocalTime.of(16, 00), 2);

    }

    @Test
    public void testSamletDosis() {
        assertEquals(9, d1.samletDosis(), 0.001);
    }

    @Test
    public void testDoegnDosis() {
        assertEquals(3, d1.doegnDosis(), 0.001);
        d1.createDosis(LocalTime.of(20, 00), -1);
        assertEquals(3, d1.doegnDosis(), 0.001);
    }

    @Test
    public void testGetType() {
        assertEquals(d1.getLaegemiddel(), l3);
    }

    @Test
    public void testDagligFast() {
        // TC1
        assertNotNull(d1);
        assertEquals(LocalDate.of(2018, 03, 06), d1.getStartDen());
        assertEquals(LocalDate.of(2018, 03, 8), d1.getSlutDen());
        DagligFast df = new DagligFast(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 02, 8));
        assertNotNull(df);

        // TC2
        assertNotNull(d2);
        assertEquals(LocalDate.of(2018, 03, 06), d2.getStartDen());
        assertEquals(LocalDate.of(2018, 03, 6), d2.getSlutDen());

    }

    @Test
    public void testCreateDosis() {
        Dosis d = d1.createDosis(LocalTime.of(16, 00), 2);
        assertNotNull(d);
        assertEquals(d1.getDoser()[2], d);
    }

    @Test
    public void testGetDoser() {
        assertNotNull(d1.getDoser());
    }

}
