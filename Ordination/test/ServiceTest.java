package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {
    private Patient p1;
    private Laegemiddel l1;
    private Service service;

    @Before
    public void setUp() throws Exception {
        service = Service.getTestService();
        p1 = service.opretPatient("090149-2529", "Ib Hansen", 87.7);

        l1 = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
    }

    @Test
    public void testOpretPNOrdination() {
        // TC1
        PN pn1 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 1);
        assertNotNull(pn1);
        assertEquals(pn1, p1.getOrdinations().get(0));

        // TC2
        PN pn2 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 06), p1, l1, 1);
        assertNotNull(pn2);
        assertEquals(pn2, p1.getOrdinations().get(1));
    }

    @Test
    public void testOpretPNOrdinationUgyldig() {
        // TC1
        try {
            @SuppressWarnings("unused")
            PN pn2 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 0);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Du skal indtaste et antal over 0", e.getMessage());
        }
        // TC2
        try {
            @SuppressWarnings("unused")
            PN pn1 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 5), p1, l1, 1);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Startdato er efter slutdato", e.getMessage());
        }

    }

    @Test
    public void testOpretDagligFastOrdination() {
        // TC1
        DagligFast d1 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1,
                1, 1, 1, 1);
        assertNotNull(d1);
        assertEquals(d1, p1.getOrdinations().get(0));
        // TC2
        DagligFast d2 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1,
                0, 0, 0, 0);
        assertNotNull(d2);
        assertEquals(d2, p1.getOrdinations().get(1));
        // TC3
        DagligFast d3 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1,
                -1, -1, -1, -1);
        assertNotNull(d3);
        assertEquals(d3, p1.getOrdinations().get(2));
        // TC4
        DagligFast d4 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 6), p1, l1,
                1, 1, 1, 1);
        assertNotNull(d4);
        assertEquals(d4, p1.getOrdinations().get(3));
    }

    @Test
    public void testOpretDagligFastOrdinationUgyldig() {
        // TC1
        try {
            @SuppressWarnings("unused")
            DagligFast d1 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 5), p1,
                    l1, 1, 1, 1, 1);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Startdato er efter slutdato", e.getMessage());
        }
    }

    @Test
    public void testOpretDagligSkaevOrdination() {
        LocalTime[] loc = new LocalTime[2];
        double[] dou = new double[2];

        // TC1
        loc[0] = LocalTime.of(8, 00);
        loc[1] = LocalTime.of(16, 30);
        dou[0] = 1;
        dou[1] = 2;
        DagligSkaev d2 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 6), p1,
                l1, loc, dou);
        assertNotNull(d2);
        assertEquals(d2, p1.getOrdinations().get(0));

        // TC2
        DagligSkaev d3 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1,
                l1, loc, dou);
        assertNotNull(d3);
        assertEquals(d3, p1.getOrdinations().get(1));
    }

    @Test
    public void testOpretDagligSkaevOrdinationUgyldig() {
        try {
            LocalTime[] loc = new LocalTime[2];
            double[] dou = new double[2];
            loc[0] = LocalTime.of(16, 30);
            dou[0] = 1;
            dou[1] = 2;

            // TC1
            @SuppressWarnings("unused")
            DagligSkaev d1 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8),
                    p1, l1, loc, dou);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Null værdier er ikke tilladt", e.getMessage());
        }

        // TC2
        try {
            LocalTime[] loc1 = new LocalTime[2];
            double[] dou1 = new double[2];

            loc1[0] = LocalTime.of(8, 00);
            loc1[1] = LocalTime.of(16, 30);
            dou1[1] = 2;

            @SuppressWarnings("unused")
            DagligSkaev d2 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 5),
                    p1, l1, loc1, dou1);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Startdato er efter slutdato", e.getMessage());
        }

        // TC3
        try {
            LocalTime[] loc = new LocalTime[1];
            double[] dou = new double[2];
            loc[0] = LocalTime.of(16, 30);
            dou[0] = 1;
            dou[1] = 2;

            // TC1
            @SuppressWarnings("unused")
            DagligSkaev d3 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8),
                    p1, l1, loc, dou);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Der er fejl i ordinationer", e.getMessage());
        }

    }

    @Test
    public void testOrdinationPNAnvendt() {
        // TC1
        PN pn3 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 1);
        service.ordinationPNAnvendt(pn3, LocalDate.of(2018, 03, 7));
        assertEquals(1, pn3.antalDage());
        assertEquals(LocalDate.of(2018, 03, 07), pn3.getDatoForDoser().get(0));

        // TC2
        PN pn4 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 1);
        service.ordinationPNAnvendt(pn4, LocalDate.of(2018, 03, 6));
        assertEquals(1, pn4.antalDage());
        assertEquals(LocalDate.of(2018, 03, 06), pn4.getDatoForDoser().get(0));

        // TC3
        PN pn5 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 1);
        service.ordinationPNAnvendt(pn5, LocalDate.of(2018, 03, 8));
        assertEquals(1, pn5.antalDage());
        assertEquals(LocalDate.of(2018, 03, 8), pn5.getDatoForDoser().get(0));
    }

    @Test
    public void testOrdinationPNAnvendtUgyldig() {
        // TC1
        try {
            PN pn5 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 1);
            service.ordinationPNAnvendt(pn5, LocalDate.of(2018, 03, 9));
            fail();
        } catch (RuntimeException e) {
            assertEquals("Dato er ikke i det gyldige interval", e.getMessage());
        }
        // TC2
        try {
            PN pn6 = service.opretPNOrdination(LocalDate.of(2018, 03, 06), LocalDate.of(2018, 03, 8), p1, l1, 1);
            service.ordinationPNAnvendt(pn6, LocalDate.of(2018, 03, 5));
            fail();
        } catch (RuntimeException e) {
            assertEquals("Dato er ikke i det gyldige interval", e.getMessage());
        }

    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel() {
        Patient p1 = service.opretPatient("121256-0512", "Jane Jensen", 63.4);
        Patient p2 = service.opretPatient("070985-1153", "Finn Madsen", 83.2);
        Patient p3 = service.opretPatient("011064-1522", "Ulla Nielsen", 59.9);

        Laegemiddel l1 = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        Laegemiddel l2 = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        Laegemiddel l3 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

        service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), p1, l2, 123);
        service.opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), p1, l1, 3);
        service.opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), p3, l3, 5);
        service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), p1, l2, 123);

        service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), p2, l2, 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
        service.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24), p2, l3, kl, an);

        assertEquals(2, service.antalOrdinationerPrVægtPrLægemiddel(50, 80, l2));
    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddelUgyldig() {
        Patient p1 = service.opretPatient("121256-0512", "Jane Jensen", 63.4);
        Patient p2 = service.opretPatient("070985-1153", "Finn Madsen", 83.2);
        Patient p3 = service.opretPatient("011064-1522", "Ulla Nielsen", 59.9);

        Laegemiddel l1 = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        Laegemiddel l2 = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        Laegemiddel l3 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

        service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), p1, l2, 123);
        service.opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), p1, l1, 3);
        service.opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), p3, l3, 5);
        service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), p1, l2, 123);

        service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), p2, l2, 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
        service.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24), p2, l3, kl, an);
        try {
            assertEquals(2, service.antalOrdinationerPrVægtPrLægemiddel(90, 80, l2));
            fail();
        } catch (RuntimeException rte) {
            assertEquals("Startvægt skal være mindre end slutvægt", rte.getMessage());
        }
    }
}