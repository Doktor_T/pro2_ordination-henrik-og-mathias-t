package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class DagligFast extends Ordination {
    private Dosis[] doser = new Dosis[4]; // DagligFast har 4 daglige doser, morgen, middag, aften og nat
    private int doserCounter = 0; // counter til at holde styr på indsættelsen i array

    public DagligFast(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
    }

    /**
     * Opretter en ny dosis, med et tidspunkt og et antal af en given mængde. Pre:
     * tid og antal er ikke null
     *
     * @tid tidspunktet hvor dosis skal gives
     * @antal størrelsen på dosis
     * @return returnerer den oprettede dosis
     */
    public Dosis createDosis(LocalTime tid, double antal) {
        Dosis d = new Dosis(tid, antal);
        doser[doserCounter] = d;
        doserCounter++;
        return d;
    }

    /**
     * @return returnerer den samlede dosis for ordinationen
     */
    @Override
    public double samletDosis() {
        return doegnDosis() * antalDage();
    }

    /**
     * @return returnerer den gennemsnitlige dags-doses for ordinationen
     */
    @Override
    public double doegnDosis() {
        double sum = 0;
        for (int i = 0; i < doserCounter; i++) {
            if (doser[i].getAntal() > 0) {
                sum += doser[i].getAntal();
            }
        }
        return sum;
    }

    /**
     * Pre: lægemiddel for ordinationen er != null
     *
     * @return returnerer navnet på det valgte lægemiddel for ordinationen
     */
    @Override
    public String getType() {
        return getLaegemiddel().getNavn();
    }

    /**
     * @return returnerer en kopi af array'et med doser
     */
    public Dosis[] getDoser() {
        return Arrays.copyOf(doser, 4);
    }
}
