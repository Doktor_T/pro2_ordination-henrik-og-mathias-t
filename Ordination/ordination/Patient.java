package ordination;

import java.util.ArrayList;

public class Patient {
    private String cprnr; // patientens CPR nummer
    private String navn; // patientens navn
    private double vaegt; // patientens vægt
    private ArrayList<Ordination> ordinationer = new ArrayList<>(); // alle de ordinationer der er tilknyttet patienten

    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
    }

    /**
     * @return patientens CPR nummer
     */
    public String getCprnr() {
        return cprnr;
    }

    /**
     * @return patientens navn
     */
    public String getNavn() {
        return navn;
    }

    /**
     * Pre: navn er ikke null
     *
     * @navn på patienten sættes
     */
    public void setNavn(String navn) {
        this.navn = navn;
    }

    /**
     * @return patientens vægt
     */
    public double getVaegt() {
        return vaegt;
    }

    /**
     * Pre: vaegt er ikke null
     *
     * @vaegt på patienten sættes
     */
    public void setVaegt(double vaegt) {
        this.vaegt = vaegt;
    }

    /**
     * @return en kopi af patientens liste over ordinationer
     */
    public ArrayList<Ordination> getOrdinations() {
        return new ArrayList<>(ordinationer);
    }

    /**
     * Pre: o er ikke null
     *
     * @o tilføjer en ordination til patienten
     */
    public void addOrdination(Ordination o) {
        ordinationer.add(o);
    }

    /**
     * Pre: o er ikke null
     * 
     * @o fjerner en ordination til patienten
     */
    public void removeOrdination(Ordination o) {
        ordinationer.remove(o);
    }

    @Override
    public String toString() {
        return navn + "  " + cprnr;
    }

}
