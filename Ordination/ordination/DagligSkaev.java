package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    private ArrayList<Dosis> doser = new ArrayList<>(); // DagligSkæv har et vilkårlig antal daglige doser

    public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
    }

    /**
     * Opretter en ny dosis, med et tidspunkt og et antal af en given mængde. Pre:
     * tid og antal er ikke null
     * 
     * @return
     *
     * @tid for hvornår den oprettede dosis skal gives
     * @antal af enheder der skal gives i forbindelse med denne dosis
     */
    public Dosis opretDosis(LocalTime tid, double antal) {
        Dosis d = new Dosis(tid, antal);
        doser.add(d);
        return d;
    }

    /**
     * @return returnerer den samlede dosis for ordinationen
     */
    @Override
    public double samletDosis() {
        return doegnDosis() * antalDage();

    }

    /**
     * @return returnerer den gennemsnitlige dags-doses for ordinationen
     */
    @Override
    public double doegnDosis() {
        double sum = 0;
        for (int i = 0; i < doser.size(); i++) {
            if (doser.get(i).getAntal() > 0) {
                sum += doser.get(i).getAntal();
            }
        }
        return sum;
    }

    @Override
    public String getType() {
        return getLaegemiddel().getNavn();
    }

    public ArrayList<Dosis> getDoser() {
        return new ArrayList<>(doser);
    }

}
