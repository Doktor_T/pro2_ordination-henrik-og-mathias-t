package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

    private double antalEnheder; // størrelsen på dosis, af den givne enhed
    private ArrayList<LocalDate> datoForDoser = new ArrayList<>(); // liste over datoer hvor ordinationen er givet
    private int antalGangeGivet; // tæller for hvor mange gange ordinationen

    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
        super(startDen, slutDen);
        this.antalEnheder = antalEnheder;
        this.antalGangeGivet = 0;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
     * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     *
     * @givesDen dato hvor dosis gives til patienten
     * @return true, hvis dosis er blevet registreret korrekt, false hvis dato for
     *         administrationen ikke er gyldig
     */
    public boolean givDosis(LocalDate givesDen) {
        if (givesDen == null) {
            return false;
        } else if (givesDen.isAfter((getStartDen().minusDays(1))) && givesDen.isBefore(getSlutDen().plusDays(1))) {
            datoForDoser.add(givesDen);
            antalGangeGivet++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return den gennemsnitlige daglige dosis, fra første gange den bliver
     *         administreret og til sidste gange
     */
    @Override
    public double doegnDosis() {
        if (antalGangeGivet > 0) {
            return samletDosis() / antalDage();
        } else {
            return 0;
        }
    }

    /**
     * @return den samlede dosis der er givet i perioden
     */
    @Override
    public double samletDosis() {
        return antalGangeGivet * antalEnheder;
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     *
     * @return
     */
    public int getAntalGangeGivet() {
        return antalGangeGivet;
    }

    /**
     * @return størrelsen på dosis, af de givne enheder
     */
    public double getAntalEnheder() {
        return antalEnheder;
    }

    /**
     * @return navnet på det valgte lægemiddel
     */
    @Override
    public String getType() {
        return getLaegemiddel().getNavn();
    }

    /**
     * Hjælpemetode
     *
     * @return den første dag ordinationen er anvendt
     */
    private LocalDate getFirstUseDate() {
        LocalDate firstUseDate = null;

        for (LocalDate ld : datoForDoser) {
            if (firstUseDate == null || ld.isBefore(firstUseDate)) {
                firstUseDate = ld;
            }
        }

        return firstUseDate;
    }

    /**
     * Hjælpemetode
     *
     * @return den sidste dag ordinationen er anvendt
     */
    private LocalDate getLastUseDate() {
        LocalDate lastUseDate = null;

        for (LocalDate ld : datoForDoser) {
            if (lastUseDate == null || ld.isAfter(lastUseDate)) {
                lastUseDate = ld;
            }
        }

        return lastUseDate;
    }

    /**
     * Antal hele dage mellem første og sidste anvendelsesdato. Begge dage
     * inklusive.
     *
     * @return antal dage ordinationen gælder for
     */
    @Override
    public int antalDage() {
        return getFirstUseDate().until(getLastUseDate()).getDays() + 1;
    }

    public ArrayList<LocalDate> getDatoForDoser() {
        return new ArrayList<>(datoForDoser);
    }
}