package ordination;

import java.time.LocalTime;

public class Dosis {
    private LocalTime tid; // tidspunktet for hvornår dosis skal gives
    private double antal; // størrelsen på dosis, af den givne enhed

    public Dosis(LocalTime tid, double antal) {
        this.tid = tid;
        this.antal = antal;
    }

    /**
     * @return størrelsen på dosis
     */
    public double getAntal() {
        return antal;
    }

    /**
     * @antal sætter den ønskede størrelse på dosis
     */
    public void setAntal(double antal) {
        this.antal = antal;
    }

    /**
     * @return tiden for dosis
     */
    public LocalTime getTid() {
        return tid;
    }

    /**
     * @tid sætter den ønskede tid på dagen for hvornår dosis skal gives
     */
    public void setTid(LocalTime tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Kl: " + tid + "   antal:  " + antal;
    }

}
