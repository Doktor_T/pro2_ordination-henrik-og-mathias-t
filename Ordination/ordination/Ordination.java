package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
    private LocalDate startDen; // start dato for ordinationen
    private LocalDate slutDen; // slut dato for ordinationen
    private Laegemiddel laegemiddel; // det ønskede lægemiddel for ordinationen

    public Ordination(LocalDate startDen, LocalDate slutDen) {
        this.startDen = startDen;
        this.slutDen = slutDen;
    }

    /**
     * Pre: laegemiddel er ikke null
     *
     * @laegemiddel sættes som det ønskede lægemiddel for ordinationen
     */
    public void setLaegemiddel(Laegemiddel laegemiddel) {
        this.laegemiddel = laegemiddel;
    }

    /**
     * @return det valgte lægemiddel for ordinationen
     */
    public Laegemiddel getLaegemiddel() {
        return this.laegemiddel;
    }

    /**
     * @return den valgte start dato for ordinationen
     */
    public LocalDate getStartDen() {
        return startDen;
    }

    /**
     * @return den valgte slut dato for ordinationen
     */
    public LocalDate getSlutDen() {
        return slutDen;
    }

    /**
     * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
     *
     * @return antal dage ordinationen gælder for
     */
    public int antalDage() {
        return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
    }

    @Override
    public String toString() {
        return startDen.toString();
    }

    /**
     * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
     *
     * @return
     */
    public abstract double samletDosis();

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen
     * er gyldig
     *
     * @return
     */
    public abstract double doegnDosis();

    /**
     * Returnerer ordinationstypen som en String
     *
     * @return
     */
    public abstract String getType();
}
